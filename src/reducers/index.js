import { combineReducers } from "redux";

const songReducer = () => {
  return [
    {
      title: "No Scrubs",
      duration: "4:05",
    },
    {
      title: "Macarina",
      duration: "3:39",
    },
    {
      title: "All Star",
      duration: "4:45",
    },
    {
      title: "Wolves",
      duration: "3.49",
    },
  ];
};

const selectedSongReducer = (selectedSong = null, action) => {
  if (action.type === "SONG_SELECTED") {
    return action.payload;
  }

  return selectedSong;
};

export default combineReducers({
  songs: songReducer,
  selectedSong: selectedSongReducer,
});
